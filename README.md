# My Electric Friend

- [My Electric Friend](#my-electric-friend)
  - [Resources](#resources)
  - [Flutter](#flutter)
    - [Why Flutter?](#why-flutter)
    - [Widget Cascading](#widget-cascading)
    - [Widget Catalog](#widget-catalog)
  - [VS Code](#vs-code)
    - [Why VS Code?](#why-vs-code)
  - [Tips](#tips)
    - [Create App Icons](#create-app-icons)
    - [Create Web Icons](#create-web-icons)
  - [Google Playstore Release](#google-playstore-release)
    - [Setup](#setup)
    - [Signing](#signing)
    - [Versioning](#versioning)
    - [Test Accounts](#test-accounts)
  - [Ad Banners](#ad-banners)
    - [Marketing ABC](#marketing-abc)
      - [RPM - Revenue Per Mille](#rpm---revenue-per-mille)
      - [CPM - Cost Per Mille](#cpm---cost-per-mille)
      - [CPC - Cost Per Click](#cpc---cost-per-click)
      - [CTR - Click Through Rate](#ctr---click-through-rate)
  - [Changing SDK](#changing-sdk)
  - [Compiling](#compiling)
    - [Flutter Doctor](#flutter-doctor)
    - [Update Flutter Dependencies](#update-flutter-dependencies)
    - [Android SDK](#android-sdk)
    - [iOS SDK](#ios-sdk)
    - [Compile to Web](#compile-to-web)
  - [Thanks](#thanks)
  

## Resources

- Google Drive, shared folder: https://drive.google.com/drive/folders/1znq1RaFOiQZqZOiG06YkyjO_sef2gbdT

## Flutter

### Why Flutter?

Single code for 3 platforms: Android, iOS and web (mobile and desktop). Flutter is a modern framework and has the support from Google. The widget concept and the typed language (Dart) makes it very productive and is imho easier than for example React Native.

Flutter Beginners Tutorial - [Flutter Crash Course for Beginners 2021 - Build a Flutter App with Google's Flutter & Dart](https://www.youtube.com/watch?v=x0uinJvhNxI)

### Widget Cascading

> "Everything is a widget"

![widget cascading](docs/widgets-cascading.svg)

The main part is happening in the *UnitsForm* widget. In there are many more widgets. Even a simple line of text is a widget.

### Widget Catalog
If you read the code you'll find several widgets used. The catalog is a good resource to check the specification: [Widget catalog](https://docs.flutter.dev/development/ui/widgets)

## VS Code

### Why VS Code?

[Visual Studio Code](https://code.visualstudio.com) is together with [Android Studio](https://developer.android.com) the common editors for Flutter projects. I prefer *Code* because I use this editor for other programming (JavaScript, TypeScript, Python) projects as well. I also like the extention concept, because I like if editors are not too big and have only the features that I like.

## Tips

- Debugger: F5 (Run > Start Debugging)

### Create App Icons
[docs](https://pub.dev/packages/flutter_launcher_icons)

1. check/ change `pubspec.yaml`
```yaml
flutter_icons:
  android: 'launcher_icon'
  ios: true
  image_path: 'assets/logo.png'
```

2. Run command
```shell
flutter pub run flutter_launcher_icons:main
```
3. Test ;-)

### Create Web Icons
[Online favicon generator](https://www.favicon-generator.org)

1. Check your logo to be a square
2. Upload, generate and download the ions
3. Check the paths in the manifest `web/manifest.json`
4. Check the paths in the html `web/index.html`
5. Build and test the icons
```shell
rm -rf build/web*
flutter build web
cd build/web
Python -m SimpleHTTPServer
```

## Google Playstore Release

### Setup

Before releasing an app for the first time, signig keys etc have to be created. See [Build and release an Android app](https://flutter.dev/docs/deployment/android)
Check these files:
- android/key.properties
- android/app/build.gradle
- android/app/src/main/AndroidManifest.xml

### Signing

https://support.google.com/googleplay/android-developer/answer/9842756?hl=en

### Versioning

General guidelines about versioning: [Android Version Guide](https://developer.android.com/studio/publish/versioning)

You don't need to modify manually your *build.gradle*.

Open your *pubspec.yaml* and you will find an entry like this: `version: 1.0.0+1`
Flutter uses semantic versioning so what do you need is modify that entry: `version: 1.0.1+2`. 
In this case the *version name* will be `1.0.1` and the *version code* will be `2`

### Test Accounts

[Setup internal test](https://support.google.com/googleplay/android-developer/answer/3131213?hl=en)

[Prepare & roll out releases](https://support.google.com/googleplay/android-developer/answer/7159011)

## Ad Banners

- [Google Ad Manager](https://developers.google.com/ad-manager/mobile-ads-sdk/android/banner)
- [Google AdMob](https://support.google.com/admob/answer/7356092?hl=en&ref_topic=7383088)
- [Admob Video about Payments](https://www.youtube.com/watch?v=dFiEv4fFf7U)
- [AdMob Payment Thresholds](https://support.google.com/admob/answer/2772208)
- [AdSense and AdMob](https://blognife.com/2017/05/25/adsense-vs-admob-cpm-rates-payments-earning-reports/)
  - AdMob, for mobile, shares 60% of the revenue
  - AdSense, for web, shares 68%
- [Flutter Ad Package](https://pub.dev/packages/ads)
- [Flutter AdMob Tutorial](https://codelabs.developers.google.com/codelabs/admob-ads-in-flutter/#0)


### Marketing ABC

#### RPM - Revenue Per Mille

The RPM indicates how much a site/ page makes revenue based on the number of impressions. The value is an *estimate* based on recent numbers.

**Example**

If a site earns $ 0.5 after 500 impressions then the RPM is $ 1

`($0.5 / 500 impressions) * 1000 = $1`
    - 
  
#### CPM - Cost Per Mille

The price for 1000 impressions.

Example

CPM of $ 0.3 means, that the advertiser pays $ 0.3 if an ad shown 1000 times.

#### CPC - Cost Per Click

#### CTR - Click Through Rate

The rate of clicks on ad per ad impressions.

## Changing SDK

Changing the SDK can be pretty painful. This is because there are many dependencies and especially to gradle. One to switch the sdk back or forth is to install everything from scratch and copy the files from your old project.

Setup flutter from scratch.

```sh
mkdir new
cd new
# make sure you are in the right channel
flutter channel
flutter create .
```
Copy files that matter:
- /lib
- /assets
- /test
- pubspec.yaml
- README.md

There are likely to be more files. But here you have to check paragraph by paragraph:
- android/app/build.gradle

- Check the web folder
- Check the icons

Find and replace: `com.example.my_electric_friend` by `net.imuks.my_electric_friend`



## Compiling

### Flutter Doctor

Before compiling you might need to check and update software dependencies. The tool for that is called: *flutter doctor*.

```sh
flutter doctor -v
```
If libs are outdated or configurations are missing, the flutter doctor will tell you and usually supply links with howtos.

### Update Flutter Dependencies

```sh
flutter packages get
```

If you happen not to have flutter doctor or errors are complaining about *flutter_tools*. Open *VS Code* un-install and install the Flutter Plugin (dart-code.flutter)

### Android SDK

```shell
flutter build apk
```

If you want to compile an Android App, you need to have *Android Studio* from Intellij installed even it you won't use it but *Visual Studio* instead. You are going to need *Android Studio* to install the *Android SDK*.

If *flutter doctor* tells you to install a certain SDK version, open *Android Studio*, go to *Preferences* and search for "Android SDK".


### iOS SDK

Afik you can do this only on a Mac and you are going to need *XCode* to be installed. Same as with the Android Studio, you won't need to use the *XCode* IDE, it's only used as tool to manage the SDK installation and it's depending libs.

The *flutter doctor* might ask you to install a certain version ov [Cocoapods](https://cocoapods.org). No worries, just follow the instructions. You are going to need the Terminal and the Ruby installer *gem*.


### Compile to Web

```shell
flutter build web
```

## Thanks

I was using this app as a template, eventhough I haven't used the DDD framework but were happy to have a running cross compiling project including web, iOs and Android: [Strategic Domain Driven Design For Improving Flutter Architecture](https://bit.ly/ddd-flutter)
