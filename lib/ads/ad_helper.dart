import 'dart:io';

class AdHelper {
  static String get bannerAdUnitId {
    if (Platform.isAndroid) {
      return 'ca-app-pub-7863064077212873/3800808351';
    } else if (Platform.isIOS) {
      return 'NULL';
    }
    throw UnsupportedError("Unsupported platform");
  }

  static String get nativeAdUnitId {
    if (Platform.isAndroid) {
      return 'ca-app-pub-7863064077212873/3800808351';
    } else if (Platform.isIOS) {
      return 'NULL';
    }
    throw UnsupportedError("Unsupported platform");
  }
}
