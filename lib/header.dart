import 'package:flutter/material.dart';

class HeaderElectricFriend extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: SafeArea(
            minimum: EdgeInsets.all(24),
            child: Column(children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(bottom: 24),
                  child: Text(
                    'Units Calculator',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'OpenSansCondensed',
                        color: Colors.grey[600],
                        fontSize: 32.0,
                        fontWeight: FontWeight.w600),
                  )),
              Image(image: AssetImage('assets/logo-imuks.jpg'), height: 80.0),
            ])));
  }
}
