import 'package:flutter/material.dart';

class Footer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 200.0,
      child: Column(
        children: <Widget>[VersionName()],
      ),
    );
  }
}

class VersionName extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Text(
      'Version 1.1.3+2',
      style: TextStyle(fontSize: 8.0),
    );
  }
}
