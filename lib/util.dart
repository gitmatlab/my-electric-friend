
roundTo2DigetsAfterPoint(double number) =>
    double.parse(number.toStringAsFixed(2));

roundTo4DigetsAfterPoint(double number) =>
    double.parse(number.toStringAsFixed(4));

// shorthand
round2(double number) => roundTo2DigetsAfterPoint(number);

final RegExp decimalNumber = RegExp(r'\d+\.?\d*$');
