import 'dart:io';

import 'package:flutter/material.dart';
import 'package:my_electric_friend/ads/admob/index.dart';
import 'package:my_electric_friend/header.dart';
import 'package:my_electric_friend/units_form.dart';
import 'package:my_electric_friend/footer.dart';

Future<void> main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'My Electric Friend',
        theme: electricFriendTheme,
        home: Scaffold(body: Body()));
  }
}

ThemeData electricFriendTheme = ThemeData(
    scaffoldBackgroundColor: Colors.grey[300],
    primaryColor: Colors.green[300],
    fontFamily: 'OpenSans',
    inputDecorationTheme: const InputDecorationTheme(
      contentPadding: EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
      labelStyle: TextStyle(
          fontFamily: 'OpenSans',
          fontWeight: FontWeight.w400,
          fontSize: 18.0,
          color: Colors.black),
      fillColor: Colors.white60,
      filled: true,
    ),
    colorScheme:
        ColorScheme.fromSwatch().copyWith(secondary: Colors.orange[200]));

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(children: <Widget>[
      (Platform.isAndroid || Platform.isIOS)
          ? AppBannerAd()
          : Container(
              height: 0,
            ),
      HeaderElectricFriend(),
      UnitsForm(),
      SizedBox(height: 100.0),
      Footer(),
    ]));
  }
}
