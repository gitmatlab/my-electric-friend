import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_electric_friend/util.dart';
import 'package:my_electric_friend/models/tariff.dart';
import 'package:my_electric_friend/models/charges.dart';
import 'package:my_electric_friend/models/units.dart';

class UnitsForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 200.0, child: Column(children: <Widget>[CashUnitsForm()]));
  }
}

class CashUnitsForm extends StatefulWidget {
  @override
  _CashUnitsFormState createState() => _CashUnitsFormState();
}

class _CashUnitsFormState extends State<CashUnitsForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a `GlobalKey<FormState>`,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  final Tariff tariff = Tariff();
  final Charges charges = Charges();
  double tenderedAlready = 0;
  double unitsAlready = 0;
  double cashTendered = 0;
  double cashAvailable = 0;
  double unitsAvailable = 0;
  late Units units;

  double vat = 0;
  double duty = 0;
  double energyCharge = 0;
  late String _vatDisplay;
  late String _dutyDisplay;
  late String _energyChargeDisplay;

  final _tenderedAlreadyController = TextEditingController();
  final _unitsAlreadyController = TextEditingController();
  final _cashTenderedController = TextEditingController();
  final _unitsAvailableController = TextEditingController();

  @override
  void initState() {
    _calculateAndUpdateValues();
    super.initState();
  }

  @override
  void dispose() {
    _tenderedAlreadyController.dispose();
    _unitsAlreadyController.dispose();
    _cashTenderedController.dispose();
    _unitsAvailableController.dispose();
    super.dispose();
  }

  void _calculateAndUpdateValues() {
    setState(() {
      tenderedAlready = _evaluateInputTenderedAlready();
      unitsAlready = _calculateUnitsAlready(tenderedAlready);
      cashTendered = _evaluateInputCashTendered();
      cashAvailable = charges.cashAvailable(cashTendered);
      units = Units(tariff, cashAvailable, unitsAlready);
      unitsAvailable = units.totalUnitsAvailable();
      vat = charges.vatCosts(cashTendered);
      duty = charges.dutyCosts(cashTendered);
      energyCharge = charges.energyCharge(cashTendered);
    });
    _updateDiplayedValues();
  }

  // Earlier payment
  double _calculateUnitsAlready(double tenderedAlready) {
    double _earlierCashAvailable = charges.cashAvailable(tenderedAlready);
    Units _earlierUnits = Units(tariff, _earlierCashAvailable, 0);
    double _unitsAlready = _earlierUnits.totalUnitsAvailable();
    return _unitsAlready;
  }

  double _evaluateInputTenderedAlready() {
    return (decimalNumber.hasMatch(_tenderedAlreadyController.value.text))
        ? double.parse(_tenderedAlreadyController.value.text)
        : 0;
  }

  double _evaluateInputCashTendered() {
    return (decimalNumber.hasMatch(_cashTenderedController.value.text))
        ? double.parse(_cashTenderedController.value.text)
        : 0;
  }

  void _updateDiplayedValues() {
    if (_tenderedAlreadyController.value.text.isEmpty ||
        _cashTenderedController.value.text.isEmpty) {
      _unitsAvailableController.value = TextEditingValue();
      _vatDisplay = "Vat:";
      _dutyDisplay = "Duty: ";
      _energyChargeDisplay = "Energy charge: ";
    } else {
      _unitsAvailableController.value = TextEditingValue(
          text: round2(units.totalUnitsAvailable()).toString());
      _vatDisplay = "Vat: " + round2(vat).toString();
      _dutyDisplay = "Duty: " + round2(duty).toString();
      _energyChargeDisplay =
          "Energy charge: " + round2(energyCharge).toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        onChanged: () => _calculateAndUpdateValues(),
        child: Column(children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(bottom: 16),
              child: TextFormField(
                  decoration: const InputDecoration(
                    labelStyle: TextStyle(fontSize: 16.0, color: Colors.black),
                    labelText: 'Tendered Already',
                    suffixText: 'ZMW',
                    counterText: "",
                  ),
                  controller: _tenderedAlreadyController,
                  keyboardType:
                      const TextInputType.numberWithOptions(decimal: true),
                  maxLength: 10,
                  autofocus: true,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(decimalNumber)
                    // WhitelistingTextInputFormatter(decimalNumber)
                  ])),
          Padding(
              padding: const EdgeInsets.only(bottom: 16),
              child: TextFormField(
                decoration: const InputDecoration(
                  labelStyle: TextStyle(fontSize: 16.0, color: Colors.black),
                  labelText: 'Cash Tendered',
                  suffixText: 'ZMW',
                  counterText: "",
                ),
                controller: _cashTenderedController,
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                maxLength: 10,
                inputFormatters: [
                  FilteringTextInputFormatter.allow(decimalNumber)
                  // WhitelistingTextInputFormatter(decimalNumber)
                ],
              )),
          Padding(
              padding: EdgeInsets.only(bottom: 16),
              child: TextField(
                enabled: false,
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                    labelText: 'Units Available',
                    labelStyle: TextStyle(fontSize: 16.0, color: Colors.white),
                    suffixStyle:
                        TextStyle(fontSize: 16.0, color: Colors.grey[300]),
                    suffixText: 'kWh',
                    // fillColor: Colors.lightGreenAccent,
                    fillColor: Color.fromRGBO(22, 123, 3, 1.0)),
                controller: _unitsAvailableController,
              )),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                width: 200,
                // color: Colors.amber[600],
                child: Padding(
                    padding: EdgeInsets.fromLTRB(8, 0, 0, 8),
                    child: Text(_vatDisplay)),
              ),
              SizedBox(
                width: 200,
                // color: Colors.amber[500],
                child: Padding(
                    padding: EdgeInsets.fromLTRB(8, 0, 0, 8),
                    child: Text(_dutyDisplay)),
              ),
              SizedBox(
                width: 200,
                // color: Colors.amber[400],
                child: Padding(
                    padding: EdgeInsets.fromLTRB(8, 0, 0, 8),
                    child: Text(_energyChargeDisplay)),
              ),
            ],
          )
        ]));
  }
}
