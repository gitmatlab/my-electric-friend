import 'package:my_electric_friend/models/tariff.dart';

class Units {
  Units(this.tariff, this.cashAvailable, this.unitsAlready);

  Tariff tariff;
  double cashAvailable = 0.0; // cash tendered - all charges and vat
  double unitsAlready = 0.0;

  double band1UnitsMax() {
    if (unitsAlready < Tariff.band1UnitsInRange) {
      return Tariff.band1UnitsInRange - unitsAlready;
    } else {
      return 0.0;
    }
  }

  double band1UnitsAvailable() {
    if (band1UnitsMax() < 0) {
      return 0;
    } else {
      if (cashAvailable / tariff.band1UnitPrice > band1UnitsMax()) {
        return band1UnitsMax();
      } else {
        return cashAvailable / tariff.band1UnitPrice;
      }
    }
  }

  double band1MoneyAvailable() {
    return band1UnitsAvailable() * tariff.band1UnitPrice;
  }

  double band2UnitsMax() {
    const double band2UnitsRangeTo =
        Tariff.band1UnitsInRange + Tariff.band2UnitsInRange;
    if (unitsAlready + band1UnitsMax() < band2UnitsRangeTo) {
      return band2UnitsRangeTo - unitsAlready - band1UnitsMax();
    } else {
      return 0.0;
    }
  }

  double band2UnitsAvailable() {
    final double unitsAvailable =
        (cashAvailable - band1MoneyAvailable()) / tariff.band2UnitPrice;
    if (unitsAvailable >= band2UnitsMax()) {
      return band2UnitsMax();
    } else {
      return unitsAvailable;
    }
  }

  double band2MoneyAvailable() {
    return band2UnitsAvailable() * tariff.band2UnitPrice;
  }

  double band3UnitsAvailable() {
    final double cash = cashAvailable - band1MoneyAvailable() - band2MoneyAvailable();
    if(cash > 0) {
      return cash / tariff.band3UnitPrice;
    } else {
      return 0;
    }
  }

  double totalUnitsAvailable() {
    return band1UnitsAvailable() + band2UnitsAvailable() + band3UnitsAvailable();
  }
}
