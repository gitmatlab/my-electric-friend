class Charges {
  Charges({this.dutyFactor = 3.00, this.vatFactor = 16.00});

  double dutyFactor;
  double dutyPercentage() => dutyFactor / 100;
  double dutyCosts(cash) => dutyPercentage() * cashAvailable(cash);

  double vatFactor;
  double vatPercentage() => vatFactor / 100;
  double vatCosts(cash) =>
      vatPercentage() * (dutyCosts(cash) + cashAvailable(cash));

  double energyFactor() =>
      1.0 +
      vatPercentage() +
      dutyPercentage() +
      vatPercentage() * dutyPercentage();
  double energyCharge(cash) => cash / energyFactor();

  double cashAvailable(double cash) => energyCharge(cash);
}
