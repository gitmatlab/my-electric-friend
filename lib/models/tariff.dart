class Tariff {
  Tariff(
      {this.band1UnitPrice = 0.47,
      this.band2UnitPrice = 0.85,
      this.band3UnitPrice = 1.94});

  static const String band1Description =
      'units between >0.00 and <= 100.00 kWh';
  static const double band1UnitsInRange = 100.00;
  double band1UnitPrice;

  static const String band2Description =
      'units between >0.00 and <= 100.00 kWh';
  static const double band2UnitsInRange = 200.00;
  double band2UnitPrice;

  static const String band3Description = 'units >300.00 kWh';
  double band3UnitPrice;
}
