import 'package:my_electric_friend/util.dart';
import 'package:test/test.dart';
import 'package:my_electric_friend/models/tariff.dart';
import 'package:my_electric_friend/models/charges.dart';
import 'package:my_electric_friend/models/units.dart';

void main() {
  group("If cash tendered is 400 and units already is 0", () {
    const double cashTendered = 400.00;
    final Charges charges = Charges();
    final double cashAvailable = charges.cashAvailable(cashTendered);
    const double unitsAlready = 0.0;
    final Tariff tariff = Tariff();
    final Units units = Units(tariff, cashAvailable, unitsAlready);

    test("then band 1 units max is 100.00", () {
      expect(units.band1UnitsMax(), 100.00);
    });
    test("then band 1 units available is 100.00", () {
      expect(units.band1UnitsAvailable(), 100.00);
    });
    test("then band 1 money available is 47.00", () {
      expect(units.band1MoneyAvailable(), 47.00);
    });
    test("then band 2 unit max is 200.00", () {
      expect(units.band2UnitsMax(), 200.00);
    });
    test("then band 2 units available is 200.00", () {
      expect(units.band2UnitsAvailable(), 200.00);
    });
    test("then band 2 money available is 170.00", () {
      expect(units.band2MoneyAvailable(), 170.00);
    });
    test("then band 3 units available is 60.7134", () {
      final unitsAvailable =
          roundTo4DigetsAfterPoint(units.band3UnitsAvailable());
      expect(unitsAvailable, 60.7134);
    });
    test("then total units available is 360.7134", () {
      final totalUnitsAvailable =
          roundTo4DigetsAfterPoint(units.totalUnitsAvailable());
      expect(totalUnitsAvailable, 360.7134);
    });
  });

  group("If cash tendered is 400 and units already is 100", () {
    const double cashTendered = 400.00;
    final Charges charges = Charges();
    final double cashAvailable = charges.cashAvailable(cashTendered);
    const double unitsAlready = 100;
    final tariff = Tariff();
    final units = Units(tariff, cashAvailable, unitsAlready);

    test("then band 1 units max is 0", () {
      expect(units.band1UnitsMax(), 0.0);
    });
    test("then band 1 units available is 0", () {
      expect(units.band1UnitsAvailable(), 0);
    });
    test("then band 1 money available is 0", () {
      expect(units.band1MoneyAvailable(), 0);
    });
    test("then band 2 unit max is 200.00", () {
      expect(units.band2UnitsMax(), 200.00);
    });
    test("then band 2 units available is 200.00", () {
      expect(units.band2UnitsAvailable(), 200.00);
    });
    test("then band 2 money available is 170.00", () {
      expect(units.band2MoneyAvailable(), 170.00);
    });
    test("then band 3 units available is 84.9402", () {
      final unitsAvailable =
          roundTo4DigetsAfterPoint(units.band3UnitsAvailable());
      expect(unitsAvailable, 84.9402);
    });
    test("then total units available is 284.9402", () {
      final totalUnitsAvailable =
          roundTo4DigetsAfterPoint(units.totalUnitsAvailable());
      expect(totalUnitsAvailable, 284.9402);
    });
  });

  group("If cash tendered is 200 and units already is 50", () {
    const double cashTendered = 200.00;
    final Charges charges = Charges();
    final double cashAvailable = charges.cashAvailable(cashTendered);
    const double unitsAlready = 50;
    final tariff = Tariff();
    final units = Units(tariff, cashAvailable, unitsAlready);

    test("then band 1 units max is 50", () {
      expect(units.band1UnitsMax(), 50);
    });
    test("then band 1 available is 50", () {
      expect(units.band1UnitsMax(), 50);
    });
    test("then band 1 money available is 23.50", () {
      expect(units.band1MoneyAvailable(), 23.50);
    });
    test("then band 2 unit max is 200.00", () {
      expect(units.band2UnitsMax(), 200.00);
    });
    test("then band 2 units available is 169.2847", () {
      final double unitsAvailable =
          roundTo4DigetsAfterPoint(units.band2UnitsAvailable());
      expect(unitsAvailable, 169.2847);
    });
    test("then band 2 money available is 143.8920", () {
      final double moneyAvailable =
          roundTo4DigetsAfterPoint(units.band2MoneyAvailable());
      expect(moneyAvailable, 143.8920);
    });
    test("then band 3 units available is 0", () {
      expect(units.band3UnitsAvailable(), 0);
    });
    test("then total units available is 219.2847", () {
      final totalUnitsAvailable =
          roundTo4DigetsAfterPoint(units.totalUnitsAvailable());
      expect(totalUnitsAvailable, 219.2847);
    });
  });

  group("If cash tendered is 25 and units already is 50", () {
    const double cashTendered = 25.00;
    final Charges charges = Charges();
    final double cashAvailable = charges.cashAvailable(cashTendered);
    const double unitsAlready = 50;
    final tariff = Tariff();
    final units = Units(tariff, cashAvailable, unitsAlready);

    test("then band 1 units max is 50", () {
      expect(units.band1UnitsMax(), 50);
    });
    test("then band 1 available is 44.5192", () {
      final double unitsAvailable =
          roundTo4DigetsAfterPoint(units.band1UnitsAvailable());
      expect(unitsAvailable, 44.5192);
    });
    test("then band 1 money available is 20.9240", () {
      final double moneyAvailable =
          roundTo4DigetsAfterPoint(units.band1MoneyAvailable());
      expect(moneyAvailable, 20.9240);
    });
    test("then band 2 unit max is 200.00", () {
      expect(units.band2UnitsMax(), 200.00);
    });
    test("then band 2 units available is 0", () {
      expect(units.band2UnitsAvailable(), 0);
    });
    test("then band 2 money available is 0", () {
      expect(units.band2MoneyAvailable(), 0);
    });
    test("then band 3 units available is 0", () {
      expect(units.band3UnitsAvailable(), 0);
    });
    test("then total units available is 44.5192", () {
      final totalUnitsAvailable =
          roundTo4DigetsAfterPoint(units.totalUnitsAvailable());
      expect(totalUnitsAvailable, 44.5192);
    });
  });
}
