import 'package:test/test.dart';
import 'package:my_electric_friend/models/charges.dart';
import 'package:my_electric_friend/util.dart';

void main() {
  group("Charges are alright", () {
    final charges = Charges();

    test("if duty costs are correct", () {
      double costs = roundTo4DigetsAfterPoint(charges.dutyCosts(100.0));
      expect(costs, 2.5109);
    });
    test("if vat costs are correct", () {
      double costs = roundTo4DigetsAfterPoint(charges.vatCosts(100.0));
      expect(costs, 13.7931);
    });
    test("if energy factor costs are correct", () {
      double costs = roundTo4DigetsAfterPoint(charges.energyCharge(100.0));
      expect(costs, 83.696);
    });
    test("if the energy charge factor is correct", () {
      double factor = roundTo4DigetsAfterPoint(charges.energyFactor());
      expect(factor, 1.1948);
    });
    test("if cash available is correct", () {
      const double cash = 400.00;
      double costs = roundTo4DigetsAfterPoint(charges.cashAvailable(cash));
      expect(costs, 334.7841);
    });
  });
}
