import 'package:test/test.dart';
import 'package:my_electric_friend/models/tariff.dart';

void main() {
  group("Tariff", () {
    test("band 1 to be defaulted to 0.47 ", () {
      final tariff = Tariff();
      expect(tariff.band1UnitPrice, 0.47);
    });
    test("band 1 can be set to 0.50 ", () {
      final tariff = Tariff(band1UnitPrice: 0.50);
      expect(tariff.band1UnitPrice, 0.5);
    });    
  });
}
